package autofillformon_url;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Utils {


public static void SleepForFewSeconds(int p_seconds){
	
	int Seconds;
	
	Seconds = p_seconds * 1000;
	
	 try{
			Thread.sleep(Seconds);
			}catch (InterruptedException e){
				System.out.println("Exception : " + e.getMessage());
			} 
		
}

public static String ReadFromCSVFile(){		
	
	// Read data from  CSV file.     
    FileReader fr = null;
    
	try {
		fr = new FileReader(new File("D:\\Amar\\AutomateFormFillWithoutLastPass\\ToBePassURLs.csv"));
	} catch (FileNotFoundException e1) {
		
		e1.printStackTrace();
	}  
	
    BufferedReader br = new BufferedReader(fr);  
    String st;  
    try {
    	
		while ((st = br.readLine()) != null) {  
			
		    System.out.println(st);  
		    return st;
		}
	} catch (IOException e) {
		
		e.printStackTrace();
	}
	return null;             
    
}

public static void Page_not_found_error(String Page_not_found_urls){
	
	// Write data from  CSV file.     
	FileWriter  fw = null;
    
	try {
		fw = new FileWriter("D:\\Amar\\AutomateFormFillWithoutLastPass\\Pagenotfound_urls.csv",true);	
	
	BufferedWriter out = new BufferedWriter(fw);
	
	//	out.write(Page_not_found_urls+""+new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString());	
	out.write(Page_not_found_urls);
	out.append('\n');
	out.close();
	
	}catch (FileNotFoundException e1) {
		
		e1.printStackTrace();
	} catch (IOException e) {
		
		e.printStackTrace();
	}  

}

public static void Captcha_containing_url(String captha_urls){
	
	// Write data from  CSV file.     
	FileWriter  fw = null;
    
	try {
		fw = new FileWriter("D:\\Amar\\AutomateFormFillWithoutLastPass\\captcha_urls.csv",true);	
	
	BufferedWriter out = new BufferedWriter(fw);
	
	//out.write(captha_urls+""+new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString());	
	out.write(captha_urls);	
	out.append('\n');
	out.close();
	
	}catch (FileNotFoundException e1) {
		
		e1.printStackTrace();
	} catch (IOException e) {
		
		e.printStackTrace();
	}  
	
}


public static void FailTestURLs(String FailedURL){
	
	// Write data from  CSV file.     
	FileWriter  fw = null;
    
	try {
		fw = new FileWriter("D:\\Amar\\AutomateFormFillWithoutLastPass\\FailedURLs.csv",true);	
	
	BufferedWriter out = new BufferedWriter(fw);
	
	out.write(FailedURL);	
	out.append('\n');
	out.close();
	
	}catch (FileNotFoundException e1) {
		
		e1.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  	
}
	

public static void PassTestURLs(String FailedURL)
{
	
	// Write data from  CSV file.     
	FileWriter  fw = null;
    
	try {
		
		fw = new FileWriter("D:\\Amar\\AutomateFormFillWithoutLastPass\\PassURLs.csv",true);	
	
		BufferedWriter out = new BufferedWriter(fw);
	
		out.write(FailedURL);	
		out.append('\n');
		out.close();
	
	}catch (FileNotFoundException e1) {
		
		e1.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  
	
}

public static void Contact_Form_missing_url(String ContactForm_missing_urls)
{
		
		// Write data from  CSV file.     
		FileWriter  fw = null;
	    
		try {
			fw = new FileWriter("D:\\Amar\\AutomateFormFillWithoutLastPass\\ContactForm_missing_urls.csv",true);	
		
		BufferedWriter out = new BufferedWriter(fw);
		
		//out.write(ContactForm_missing_urls+""+new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString());	
		out.write(ContactForm_missing_urls);
		out.append('\n');
		out.close();
		
		}catch (FileNotFoundException e1) {
			
			e1.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}  
		
}


	
	
}
