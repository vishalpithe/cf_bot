
package autofillformon_url;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mongoClient.singletone.GetMongoClient;
import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;


public class AutoFillForm {

//public static WebDriver driver1;
	public static ChromeDriver driver;
	public static MongoClient mongo=GetMongoClient.getConnection();
	public static DB db=mongo.getDB("marketing");
	public static DBCollection coll=db.getCollection("contactUs");
	public static Cursor cursor;
	static SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
//	public static FirefoxDriver driver;
	    
	//It will start the crome browser having roboForm extension
		@BeforeTest
		public void LaunchBrowser() 
		{			
			//--------------------------- Chrome -----------------------------------	
			
			 Scanner sc=new Scanner(System.in);
			 System.out.println("Enter Form Filling Day target  : ");
			 int limit=sc.nextInt();
			 
			 if(limit>=1)
				 cursor=coll.find(new BasicDBObject("status","notCrawled")).limit(limit);
			 else
			 {
				 System.err.println("Please enter valid limit..");
				 System.err.println("Wait for 5 sec Restarting........ ");
				 try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
						e.printStackTrace();
				}
				 LaunchBrowser();
			 }
			 ChromeOptions options = new ChromeOptions();			 
			 options.addExtensions(new File("C:\\Users\\Administrator\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Extensions\\pnlccmojcmeohlpggmfnbbiapkmbliob\\7.9.21.5_1.crx"));//this adds extension to the browser
			 System.setProperty("webdriver.chrome.driver",".\\chromedriver.exe");			
			 driver = new ChromeDriver(options); 
			 driver.manage().window().maximize();
			 			
		}
		
		
		//Close down the browser
		@AfterTest
		public void close()
		{
			driver.quit();
		}
		
		
		@Test(priority=1, enabled=true)
		public void fillForm()
		{
			 int count1=1;
			 int count2=1;
			 int count3=1;
			 int count4=1;
			 int count5=1;
			 int count=1;
			 int count6=1;
			 
			 
	        
			
	        /*
	         
            // Read data from  CSV file.     
	        FileReader fr = null;
	         try 
			{
				fr = new FileReader(new File("D:\\Amar\\AutomateFormFillWithoutLastPass\\ToBePassURLs.csv"));//This will take URLs to be passed one by one to the browser for form filling
			} 
			
			catch (FileNotFoundException e1)
			{
				
				e1.printStackTrace();
			}  			
	        BufferedReader br = new BufferedReader(fr);
	        */
	        
	        
	        DBObject dbObject;
	        String ulr="";  
	        
	        while (cursor.hasNext())
	        { 
	        	dbObject=cursor.next();
	        	System.out.println(dbObject.get("url").toString());
	        	try
	        	{
	        		ulr=dbObject.get("url").toString();
	        		System.out.println(count+" "   +ulr + new Date().getSeconds());

	        		/*
	        		 *Fetching url in driver
	        		 */

	        		driver.get(ulr);	
	        		System.out.println("Link fetched in driver...!" +new Date().getSeconds() );

	        		//count++;
	        		
	        		URL url = new URL(ulr);
	        		HttpURLConnection http = (HttpURLConnection)url.openConnection();
	        		int statusCode = http.getResponseCode();//it gives http response code 

	        		//Check for HttpStatus Code 
	        		if(statusCode==404)
	        		{
	        			System.out.println("Page Not Found and count of page not found pages is::" +count1);
	        			coll.update(dbObject, new BasicDBObject("$set",new BasicDBObject("status","pageNotFound").append("crawledDate", ft.format(new Date()))));
	        			//Utils.Page_not_found_error(ulr);
	        			count1++;
	        		}

	        		// verifiy page not found error
	        		else  if(driver.getPageSource().contains("Not Found") || driver.getPageSource().contains("Error 404")
	        				|| driver.getPageSource().contains("404 Error")|| driver.getPageSource().contains("404 error")
	        				||driver.getPageSource().contains("404 - Category not found"))				    
	        		{
	        			System.out.println("Page Not Found and count of page not found pages is::" +count1);
	        			
	        			coll.update(dbObject, new BasicDBObject("$set",new BasicDBObject("status","pageNotFound").append("crawledDate", ft.format(new Date()))));
	        			
	        			Utils.Page_not_found_error(ulr);
	        			count1++;

	        		}
	        		// verifiy if captcha is present in the form
	        		else if(driver.getPageSource().contains("Please enter the code above (field is not case sensitive)")
	        				|| driver.getPageSource().contains("Security")
	        				|| driver.getPageSource().contains("captcha)")||driver.getPageSource().contains("Captcha)") 
	        				|| driver.getPageSource().contains("Please answer the following:")
	        				|| driver.getPageSource().contains("Enter Words from Image Below:")
	        				|| driver.getPageSource().contains("Enter the code below")||driver.getPageSource().contains("What is 5+3")
	        				|| driver.getPageSource().contains("Please enter the above security code here")
	        				|| driver.getPageSource().contains("Digite o código da imagem:") || driver.getPageSource().contains("Sticka") 
	        				|| driver.getPageSource().contains("Enter The Words") || driver.getPageSource().contains("Type the characters you see here") 
	        				|| driver.getPageSource().contains("Enter Security Code") || driver.getPageSource().contains("Enter Word Verification in box below") 
	        				|| driver.getPageSource().contains("Please answer the following: ") 
	        				|| driver.getPageSource().contains("Please enter the code above") ||driver.getPageSource().contains("Please enter the code below")
	        				|| driver.getPageSource().contains("Type the letters you see in the image below.") 
	        				|| driver.getPageSource().contains("Please type the code below to let us know you're not a robot") 
	        				|| driver.getPageSource().contains("Please enter the following characters and numbers you see*")
	        				|| driver.getPageSource().contains("Enter the words above") || driver.getPageSource().contains("Security code") 
	        				|| driver.getPageSource().contains("Verification") || driver.getPageSource().contains("Security Code check") 
	        				|| driver.getPageSource().contains("Type the text")|| driver.getPageSource().contains("Validation")
	        				|| driver.getPageSource().contains("Enter text shown in the picture") || driver.getPageSource().contains("Confirmação de Segurança") 
	        				|| driver.getPageSource().contains("Digite o código abaixo") || driver.getPageSource().contains("Masukkan kode di atas") 
	        				|| driver.getPageSource().contains("Por favor, introduzca el código mostrado") 
	        				|| driver.getPageSource().contains("Please enter any two digits with no spaces")
	        				|| driver.getPageSource().contains("Verificador") || driver.getPageSource().contains("Senha") 
	        				|| driver.getPageSource().contains("Digite o código na caixa abaixo") || driver.getPageSource().contains("Questão de Matemática") 
	        				|| driver.getPageSource().contains("Por Favor confirma o Captcha") || driver.getPageSource().contains("Entre as letras conforme mostradas na caixa acima.") 
	        				|| driver.getPageSource().contains("Please enter this security confirmation code")  || driver.getPageSource().contains("Enter the code above here") 
	        				|| driver.getPageSource().contains(" Isto é para evitar Entradas de SPAM.") || driver.getPageSource().contains("Bitte Code wie oben eingeben") 
	        				|| driver.getPageSource().contains("Enter Code") || driver.getPageSource().contains("Enter the code shown above") 
	        				|| driver.getPageSource().contains("Digite o código exibido na imagem abaixo")
	        				|| driver.getPageSource().contains("Imagem de Segurança")|| driver.getPageSource().contains("Verification image")
	        				|| driver.getPageSource().contains("Isto é para evitar Entradas de SPAM.") || driver.getPageSource().contains("入力してください。")  
	        				|| driver.getPageSource().contains("请输入以上验证码（英文字母不分大小写）")   || driver.getPageSource().contains("Validation code") 
	        				|| driver.getPageSource().contains("Kод")  || driver.getPageSource().contains("Fill captcha")  
	        				|| driver.getPageSource().contains("Please type the code below to let us know you're not a robot")
	        				|| driver.getPageSource().contains("入力してください")   || driver.getPageSource().contains("Please enter the following characters and numbers you see")  
	        				|| driver.getPageSource().contains("Please enter any two digits with no spaces (Example: 12)") 
	        				|| driver.getPageSource().contains("We welcome any questions/comments you may have")   || driver.getPageSource().contains("Enter Words from Image Below")  
	        				|| driver.getPageSource().contains("Please type the letters below") || driver.getPageSource().contains("I'm not a robot")
	        				)			        
	        		{
	        			System.out.println("Capcha is present and count of capcha pages::"+count2);
	        			
	        			coll.update(dbObject, new BasicDBObject("$set",new BasicDBObject("status","captchaPresent").append("crawledDate", ft.format(new Date()))));
	        			
	        			//Utils.Captcha_containing_url(ulr);//if URL contains captcha place it in captcha_urls.csv file
	        			count2++;

	        		}



	        		else if(driver.getPageSource().contains("<form"))//Check if Contact us page is having contact form
	        		{
	        			try
	        			{    
	        				System.out.println("Form found on the url...");
	        				//Robot class is used for mouse and keyboard handling automatically
	        				Robot robot = new Robot();  // Robot class throws AWT Exception  
	        				//robot.mouseMove(950, 32);  // change this according to the resolution changes

	        				robot.mouseMove(1316, 45);	//Click roboform extention
	        				robot.mousePress(InputEvent.BUTTON1_MASK);
	        				robot.mouseRelease(InputEvent.BUTTON1_MASK);
	        				System.out.println("Clicket on roboform : "+new Date().getSeconds());
	        				Thread.sleep(200);
	        				//robot.mouseMove(810, 230);// change this according to the resolution changes

	        				robot.mouseMove(1190, 234);	//right click stive
	        				Thread.sleep(2000);
	        				robot.mousePress(InputEvent.BUTTON3_MASK);
	        				robot.mouseRelease(InputEvent.BUTTON3_MASK);

	        				Thread.sleep(200);

	        				robot.mouseMove(1250, 380); //click fillform
	        				robot.mousePress(InputEvent.BUTTON1_MASK);
	        				robot.mouseRelease(InputEvent.BUTTON1_MASK);

	        				Thread.sleep(700);

	        				robot.mouseMove(600, 380); //click stive
	        				robot.mousePress(InputEvent.BUTTON1_MASK);
	        				robot.mouseRelease(InputEvent.BUTTON1_MASK);

	        				Thread.sleep(300);

	        				robot.mouseMove(700, 480);	//click fill&submit
	        				//Thread.sleep(2000);
	        				robot.mousePress(InputEvent.BUTTON1_MASK);
	        				robot.mouseRelease(InputEvent.BUTTON1_MASK);
	        				System.out.println("Form submited : "+new Date().getSeconds());
	        				// Thread.sleep(2000);
	        				robot.keyPress(KeyEvent.VK_ENTER);
	        				robot.keyRelease(KeyEvent.VK_ENTER);
	        				Thread.sleep(500);

	        				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	        				String redirected_url = driver.getCurrentUrl();				            	
	        				System.out.println("Redirected "
	        						+ "to::"+redirected_url);
	        				Document doc=Jsoup.connect(redirected_url).userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0").get();
	        				String pageSource=doc.toString();

	        				SleepForFewSeconds(5);

	        				if(pageSource.contains("" ) || pageSource.contains("success" ) || pageSource.contains("Successfully" )
	        						|| pageSource.contains("Thank you for contacting")||pageSource.contains("Thank you! Your message has been sent.")
	        						|| pageSource.contains("Your successfully" ) || pageSource.contains("Thanks" ) ||pageSource.contains("thanks" )
	        						|| pageSource.contains("Successenquiry has been sent, we will be in touch very soon.." )	 
	        						|| pageSource.contains("Thank" )||pageSource.contains("sent") || pageSource.contains("thnx")
	        						|| pageSource.contains("Your message has been sent")||pageSource.contains("Your message has been sent.")
	        						|| pageSource.contains("Your message has been sent; thank you.")
	        						|| pageSource.contains("has been sent" ) || pageSource.contains("Thank you") 
	        						|| pageSource.contains("Message Sent") || pageSource.contains("shortly") 
	        						|| pageSource.contains("mensaje") || pageSource.contains("sucesso") 
	        						|| pageSource.contains("Dank u dat u contact met") || pageSource.contains("received") 
	        						|| pageSource.contains("Thanks! Your email was sent")|| pageSource.contains("Thank You") 
	        						|| pageSource.contains("Success Your Message was sent, Thank you") ||pageSource.contains("Thank you for your interest")
	        						|| pageSource.contains("Notification: Your message has been sent successfully.") 
	        						|| pageSource.contains("Thank you for your interest in our products") 
	        						|| pageSource.contains("Thanks for contacting us! We will get back in touch as soon as we can."+ "If you prefer you can also call (877) 433-5766 For Immediate Assistance Calling is Always Better!")
	        						|| pageSource.contains("Your message has been sent to") ||pageSource.contains("Your message was sent successfully. Thanks.")
	        						|| pageSource.contains("Thank you for your comments. We will direct your email to the appropriate staff member and they will answer your question as soon as possible.")
	        						|| pageSource.contains("Thank You for your feedback - your e-mail will be answered as soon as possible!")
	        						|| pageSource.contains("A Customer Service Representative will be in contact with you as soon as possible.")  
	        						|| pageSource.contains("We look forward to welcoming you to")   || pageSource.contains("Thank you for visiting")
	        						|| pageSource.contains("Message Sent") || pageSource.contains("Thank you!")|| pageSource.contains("Thank You!")
	        						|| pageSource.contains("Thankyou for signing up to our Newsletter.")  || pageSource.contains("We are always here to help.")
	        						|| pageSource.contains("Your message was sent successfully, and you should be hearing from us soon! Please note we will add your email to The Center for Mind-Body Medicine newsletter/email list so we can keep you posted about our program dates & locations, and our work. You can easily opt out anytime with one click. Thank you.")
	        						|| pageSource.contains("Thank you for your comments. We will direct your email to the appropriate staff member and they will "
	        								+ "answer your question as soon as possible.")  || pageSource.contains("If you cannot wait for your response, please do call us at immediately")
	        						|| pageSource.contains("Thank you. Your message has been sent and you should expect a response shortly. "
	        								+ "Very often, the entire practice staff is occupied serving clients in our office. Please allow 2 business days for a thoughtful response.")
	        						|| pageSource.contains("Your message was sent successfully, and you should be hearing from us soon! Please note we will add your email "
	        								+ "to The Center for Mind-Body Medicine newsletter/email "
	        								+ "list so we can keep you posted about our program dates & locations, and our work. You can easily opt out anytime with one click. Thank you.")
	        						|| pageSource.contains("Thank you for Contact us at")  || pageSource.contains("Department of Transportation. We Will Contact You Soon.")  
	        						|| pageSource.contains("If appropriate someone from the district office will be in contact with you shortly")
	        						|| pageSource.contains("Thank you! Your submission has been sent.")
	        						|| pageSource.contains("Thank you for contacting us. We'll answer you as soon as possible.")   || pageSource.contains("YOUR MESSAGE WAS SENT")   
	        						|| pageSource.contains("Please feel free to continue browsing our website.")
	        						|| pageSource.contains("Your message has been received, and we will be in touch with you soon.")   
	        						|| pageSource.contains("We will respond as soon as practical")
	        						|| pageSource.contains("The information you submit on this form will be used solely for the purpose replying to your comments. "
	        								+ "If you have questions about our Privacy Policy, please contact our Privacy Officer.")
	        						|| pageSource.contains("Thank you for your submission")   || pageSource.contains("If appropriate someone from the district "
	        								+ "office will be in contact with you shortly")
	        						|| pageSource.contains("Form filled successfully")
	        						|| pageSource.contains("Thank you for your e-mail")  || pageSource.contains("Thank you for your interest in")
	        						|| pageSource.contains("Thank you, your submission has been received")  || pageSource.contains("Thank you for your interest in our")
	        						|| pageSource.contains("Thank you so much for your inquiry, we will contact you as soon as possible")  || pageSource.contains("Kind regards")
	        						|| pageSource.contains("A member of our Team will be in touch shortly with a reply")  || pageSource.contains("Thank you for your enquiry")
	        						|| pageSource.contains("Thank you for your enquiry. We will be in touch soon")  || pageSource.contains("We look forward to welcoming you")
	        						|| pageSource.contains("Thank you for contacting us. Your message has been submitted. If needed, we may contact you at the email address you have provided.")
	        						|| pageSource.contains("Thank you for contacting us!")  || pageSource.contains("Your enquiry has been sent successfully.")
	        						|| pageSource.contains("Thank you! Your message has been submitted.")   || pageSource.contains("Send us a message")
	        						|| pageSource.contains("Your enquiry has been successfully sent")  || pageSource.contains("Thank you for your e-mail.")
	        						|| pageSource.contains("Message Sent Successfully!")  || pageSource.contains("Your Form submission has been received and will be processed shortly.")
	        						|| pageSource.contains("We have received your request. One of our representatives will contact you with the information requested.")
	        						|| pageSource.contains("We've added you to the mailing lists you selected.")  || pageSource.contains("Your email was successfully sent. We will be in touch soon.")
	        						|| pageSource.contains("Thanks for your message. We will get back to you ASAP")   || pageSource.contains("Thank you for your submission.")
	        						|| pageSource.contains("Thank you for taking the time to contact")   || pageSource.contains("We do appreciate all feedback.")  
	        						|| pageSource.contains("We look forward to speaking with you.")||pageSource.contains("Thank you for contacting us.")
	        						|| pageSource.contains("Thank you for enquiry. We will get back to you!!")
	        						|| pageSource.contains("Thank you. We have received your request. One of our representatives will contact you with the information requested.")
	        						||pageSource.contains("Thank you. Your message has been sent and you should expect a response shortly. Very often, the entire practice staff is occupied serving clients in our office. Please allow 2 business days for a thoughtful response.")
	        						||pageSource.contains("Thank you for Contact us at Los Angeles, Department of Transportation. We Will Contact You Soon.")

	        						|| pageSource.contains("Your Form has been successfully sent. We will contact you as soon as possible with a response.")
	        						|| pageSource.contains("Thank you for your interest in C3, the leading provider of software for the Community Association Industry. A representative will contact you shortly.")



	        						)
	        				{
	        					System.out.println("Success Message is Present and count of successful pages::"+count3);
	        					coll.update(dbObject, new BasicDBObject("$set",new BasicDBObject("status","passURL").append("crawledDate", ft.format(new Date()))));
	        					//Utils.PassTestURLs(ulr); 
	        					count3++;

	        				}

	        				else{

	        					System.out.println("Success Message not Present / form is not submitted successfully!"+count4);
	        					coll.update(dbObject, new BasicDBObject("$set",new BasicDBObject("status","failedURL").append("crawledDate", ft.format(new Date()))));
	        					//Utils.FailTestURLs(ulr); 			        		
	        					count4++;
	        				}

	        			} 
	        			catch (InterruptedException e) 
	        			{
	        				// TODO Auto-generated catch block
	        				e.printStackTrace();
	        				coll.update(dbObject, new BasicDBObject("$set",new BasicDBObject("status","failedURL").append("crawledDate", ft.format(new Date()))));
	        				//Utils.FailTestURLs(ulr); 
	        			}  
	        			catch (AWTException e) 
	        			{
	        				// TODO Auto-generated catch block
	        				e.printStackTrace();
	        				coll.update(dbObject, new BasicDBObject("$set",new BasicDBObject("status","failedURL").append("crawledDate", ft.format(new Date()))));
	        				//Utils.FailTestURLs(ulr); 
	        			}

	        			//  Boolean isPresent = driver.findElements(By.cssSelector("[type='submit']")).size() > 0;
	        			//  Boolean isOtherPresent=driver.findElements(By.cssSelector("[value='Submit']")).size() > 0;

	        			// Boolean isOtherAltPresent=driver.findElements(By.cssSelector("[alt='Submit']")).size() > 0;
	        			//  isOtherPresent==true*/			     


	        		}

	        		else
	        		{
	        			System.out.println("Form not present on the webpage::"+count6);
	        			coll.update(dbObject, new BasicDBObject("$set",new BasicDBObject("status","contactFormMissing").append("crawledDate", ft.format(new Date()))));
	        			//Utils.Contact_Form_missing_url(ulr); 	
	        			count6++;
	        		}

	        	}//end try after while
	        	catch(Exception e)
	        	{
	        		e.printStackTrace();
	        		coll.update(dbObject, new BasicDBObject("$set",new BasicDBObject("status","error").append("crawledDate", ft.format(new Date()))));
	        		//Utils.FailTestURLs(ulr); 
	        		continue;

	        	}
	        	count++;
	        }//end of while

	        
	        System.out.println("Total runnable urls::"+count);
		} 

		
		static void SleepForFewSeconds(int p_seconds){
			
			int Seconds;
			
			Seconds = p_seconds * 1000;
			
			 try{
					Thread.sleep(Seconds);
					}catch (InterruptedException e){
						System.out.println("Exception : " + e.getMessage());
					} 
				
		}
		
}
