package com.mongoClient.singletone;

import com.mongodb.MongoClient;

public class GetMongoClient {

	
		private static final MongoClient instance = new MongoClient("localhost", 27017);
		//private static final MongoClient instance = new MongoClient("localhost", 27017);
		
	    //private constructor to avoid client applications to instantiate
	    private GetMongoClient(){}

	    public static MongoClient getConnection(){
	        return instance;
	    }

}
